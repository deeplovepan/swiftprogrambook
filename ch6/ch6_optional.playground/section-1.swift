
// 6-2
var age:Int?
var age1:Int? = nil
//var age2:Int = nil

// 6-3
var age3:Int? = 18
age3 = age3! + 1
var age4:Int = 18

// 6-4
age3 = 18
if age3 != nil {
    age3 = age3! + 1
}

if age3 > 18 {
    var message = "年滿18歲，可以在24小時的誠品過夜了"
}
else {
    var message = "未滿18歲，請在半夜12點前回家"
}

// 6-5
age3 = 18
if let ageNumber = age3 {
    age3 = ageNumber + 1
}
else {

}

// 6-6
var age5:Int! = 18
age5 = age5 + 1
age5 = nil
if age5 != nil {
    age5 = age5 + 1
}

// 6-7
var age6:Int? = nil
if age6 == nil {
    var ageNumber = 18
    var message = "我今年剛滿\(ageNumber)歲"
}
else {
    var message = "我今年剛滿\(age6!)歲"

}
age6 = 3
var ageNumber = age6 ?? 18
age6 = nil
ageNumber = age6 ?? 18



