
// 10-1
class Baby {
    var age = 1
    
    func sleep() {
        var message = "吃飽就想睡"
    }
}

class SuperBaby:Baby {
    
}

var cuteBaby = SuperBaby()
var message = "我今年\(cuteBaby.age)歲"
cuteBaby.sleep()

// 10-2
class SuperBaby2:Baby {
    
    override func sleep() {
        var message = "一天只睡一小時"
    }
}
var cuteBaby2 = SuperBaby2()
cuteBaby2.sleep()


class Baby2 {
    var age = 1
    
    func sleep() -> String {
        return "吃飽就想睡"
    }
}

class SleepingBeauty:Baby2 {
    
    override func sleep() -> String {
        var message = super.sleep()
        message = message + " 睡飽繼續睡"
        return message
    }
}

var cuteBaby3 = SleepingBeauty()
cuteBaby3.sleep()

class Square {
    var x:Double = 0
    var y:Double = 0
    var width:Double = 9
    var centerX:Double {
        get {
            return x + width/2
        }
        set {
            x = newValue - width/2
        }
    }
}

class SpecialSquare:Square {
    
    override var centerX:Double {
        get {
            return x + width/3
        }
        set {
            x = newValue - width/3
        }
    }
}

var specialSquare = SpecialSquare()
specialSquare.centerX
specialSquare.centerX = 6

class Baby3 {
    var name = "小公主"
    
}

class SleepingBeauty3:Baby3 {
    
    override var name:String {
        get {
            return "美麗的" + super.name
        }
        set {
            
        }
    }
}

var beauty = SleepingBeauty3()
var name = beauty.name

class Baby4 {
    var message:String = ""
    var name:String = "小彼得" {
        willSet {
            message += "寶寶即將取名!"
        }
        didSet {
            message += "寶寶取好名字了!"

        }
    }
}

class SleepingBeauty4:Baby4 {
    
    override var name:String {
        willSet {
            message += "睡美人即將取名!"
        }
        didSet {
            message += "睡美人取好名字了!"
            
        }
    }

}
var beauty4 = SleepingBeauty4()
beauty4.name = "灰姑娘"

class Baby5 {
    var message:String = ""
    var name:String = "小彼得"
}

class SleepingBeauty5:Baby5 {
    
    override var name:String {
        willSet {
            message += "睡美人即將取名!"
        }
        didSet {
            message += "睡美人取好名字了!"
            
        }
    }
}

class Square2 {
    var x:Double = 0
    var y:Double = 0
    var width:Double = 10
    var centerX:Double {
        get {
            return x + width/2
        }
        set {
            println("設定Square的centerX")
            x = newValue - width/2
        }
    }
}

class SpecialSquare2:Square2 {
    
    override var centerX:Double {
        willSet {
            println("centerX即將改變")
        }
        didSet {
            println("centerX已改變")
            
        }
    }
}
var mySquare = SpecialSquare2()
mySquare.centerX = 10

class SpecialSquare3:SpecialSquare2 {
    
    override var centerX:Double {
        get {
            return x + width/3
        }
        set {
            println("設定SpecialSquare3的centerX")
            x = newValue - width/3
            super.centerX = newValue
        }
    }
}
var mySquare2 = SpecialSquare3()
mySquare2.centerX = 10

// 10-3
final class Baby6 {
    var name = "小公主"
}

//class SleepingBeauty: Baby {
    
//}

// 10-4
class Baby7 {
    var age:Int
    func sing() {
        var message = "在樹上唱歌"
    }
    init() {
        self.age = 18
        sing()
    }
}

class SuperBaby7: Baby7 {
    var magic:Int
    
    init(magic:Int) {
        self.magic = magic
        super.init()
        self.age = 1000
    }
}

var cuteBaby7 = SuperBaby7(magic: 3)

class Baby8 {
    var age:Int
    var name:String
    
    init(age:Int, name:String) {
        self.age = age
        self.name = name
    }
    
    convenience init(name:String) {
        self.init(age:1, name:name)
        
    }
}
var cuteBaby8 = Baby8(name: "peter")
cuteBaby8 = Baby8(age:11, name: "andy")


class Baby9 {
    var age:Int
    
    init(age:Int) {
        self.age = age
    }
}

class SuperBaby9:Baby9 {
    
}

var cuteBaby9 = SuperBaby9(age: 10)

class SuperBaby10:Baby9 {
    
    var magic: Int
    
    init(magic:Int) {
        self.magic = magic
        super.init(age: 3)
    }
}
var cuteBaby10 = SuperBaby10(magic: 10)
//cuteBaby10 = SuperBaby10(age: 10)

class Baby11 {
    var age:Int
    
    init(age:Int) {
        self.age = age
    }
    
    convenience init() {
        self.init(age:18)
    }
}

class SuperBaby11:Baby11 {

}

var cuteBaby11 = SuperBaby11()

class Baby12 {
    var age:Int
    var name:String
    
    init(age:Int) {
        self.age = age
        self.name = "peter"
    }
    
    init(name:String, age:Int) {
        self.name = name
        self.age = age
    }
    
    convenience init() {
        self.init(name:"peter", age:18)
    }
}

class SuperBaby12:Baby12 {
    
    override init(age:Int) {
        super.init(age: 10)
    }
    
    override init(name:String, age:Int) {
        super.init(name: "andy", age: 3)
    }
}

var cuteBaby12 = SuperBaby12()

// 10-5
class Baby13 {
    var age:Int
    
    required init() {
        self.age = 3
    }
}

class SuperBaby13:Baby13 {
    
    init(age:Int) {
        super.init()
        self.age = age
    }
    
    required init() {
        super.init()
        self.age = 10
    }
}
var cuteBaby13 = SuperBaby13(age:100)
cuteBaby13 = SuperBaby13()

// 10-6
class Baby14 {
    class func run() {
        var message = "跑步"
        self.drink()
        Baby14.drink()
    }
    
    class func drink() {
        var message = "喝水"
        
    }
    func sleep() {
        
    }
}
Baby14.run()


