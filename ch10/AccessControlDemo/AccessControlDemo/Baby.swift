//
//  Baby.swift
//  AccessControlDemo
//
//  Created by Peter Pan on 3/1/15.
//  Copyright (c) 2015 Peter Pan. All rights reserved.
//

import Foundation

class Baby {
    private var age = 1
    func sleep() {
        self.eat()
    }
    private func eat() {
        
    }
}