//
//  ViewController.swift
//  AccessControlDemo
//
//  Created by Peter Pan on 3/1/15.
//  Copyright (c) 2015 Peter Pan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        var cuteBaby = Baby()
        cuteBaby.age = 18
        cuteBaby.sleep()
        cuteBaby.eat()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

