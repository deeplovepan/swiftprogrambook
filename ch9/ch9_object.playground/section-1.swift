
// 9-2 ~ 9-4
class Baby {
    var age = 1
    var name = "小彼得"
}


var cuteBaby = Baby()
var message = "我是\(cuteBaby.name)寶寶，一出生就\(cuteBaby.age)歲"

cuteBaby.name = "天然呆"
cuteBaby.age = 100
 message = "我是\(cuteBaby.name)寶寶，一出生就\(cuteBaby.age)歲"

class Baby2 {
    var age:Int?
    var name:String?
}

// 9-5
let cuteBaby2 = Baby()
cuteBaby2.age = 2
//cuteBaby2 = Baby()

// 9-6
class Baby3 {
    var age:Int
    var name:String
    
    init() {
        age = 1
        name = "小彼得"
    }
}
var cuteBaby3 = Baby3()

// 9-8
class Baby4 {
    var age:Int
    var name:String
    
    init(age:Int, name:String) {
        self.age = age
        self.name = name
    }
    init() {
        age = 1
        name = "小彼得"
    }
}
var cuteBaby4 = Baby4(age: 1, name: "小彼得")

class Baby5 {
    var age:Int
    var name:String
    
    init(_ age:Int, name:String) {
        self.age = age
        self.name = name
    }
    
}
var cuteBaby5 = Baby5(1, name: "小彼得")

// 9-9
class Baby6 {
    var age:Int
    var name:String
    
    init?(age:Int, name:String) {
        self.age = age
        self.name = name
        if age < 0 {
            return nil
        }
    }
}
var cuteBaby6 = Baby6(age:-1, name: "小彼得")
if let baby = cuteBaby6 {
    var message = "\(baby.age)歲的\(baby.name)"
}
else {
    var message = "生寶寶失敗"
}

// 9-10
class Square {
    var x:Double = 0
    var y:Double = 0
    var width:Double = 10
    var centerX:Double {
        get {
            return x + width/2
        }
        set(newCenter) {
            x = newCenter - width/2
        }
    }
}
var superSquare = Square()
superSquare.centerX
superSquare.centerX = 10

class Square2 {
    var x:Double = 0
    var y:Double = 0
    var width:Double = 10
    var centerX:Double {
        get {
            return x + width/2
        }
        set {
            x = newValue - width/2
        }
    }
}

class Square3 {
    var x:Double = 0
    var y:Double = 0
    var width:Double = 10
    var centerX:Double {
        
        return x + width/2
    
    }
}

// 9-11
class Square4 {
    var x:Double = 0 {
        willSet(newX) {
            var message = "x即將從\(x)變成\(newX)"
         }
        didSet(oldX) {
            var message = "x已從\(oldX)變成\(x)"

        }
    }
    var y:Double = 0
    var width:Double = 10
    var centerX:Double {
        get {
            return x + width/2
        }
        set {
            x = newValue - width/2
        }
    }

}
var superSquare4 = Square4()
superSquare4.x = 10
superSquare4.x = 10
superSquare4.centerX = 10

class Square5 {
    var x:Double = 0 {
        willSet {
            var message = "x即將從\(x)變成\(newValue)"
        }
        didSet {
            var message = "x已從\(oldValue)變成\(x)"
            
        }
    }
    var y:Double = 0
    var width:Double = 10
    var centerX:Double {
        get {
            return x + width/2
        }
        set {
            x = newValue - width/2
        }
    }
    
}

// 9-12
class Dog {
    init() {
        var message = "小狗誕生"
    }
}

class Baby7 {
    var age:Int
    var name:String
    lazy var dog = Dog()
    
    init() {
        name = "小彼得"
        age = 10
    }
}

var cuteBaby7 = Baby7()
cuteBaby7.dog

// 9-13
class Baby8 {
    var age = 1
    var name = "小彼得"
    
    func eat() {
        var message = "睡飽了就想吃"
    }
    
    func sleep(hour:Int, min:Int) {
        var message = "吃飽了就想睡"
    }
}
var cuteBaby8 = Baby8()
cuteBaby8.sleep(12, min: 30)

// 9-14
var cuteBaby9:Baby8? = Baby8()
cuteBaby9!.age = 2
cuteBaby9!.eat()
cuteBaby9 = nil
if cuteBaby9 != nil {
    cuteBaby9!.age = 2
}
if let baby = cuteBaby9 {
    baby.age = 2
}

// 9-15
class Food {
    var name:String = "milk"
}

class Baby10 {
    
    func eat(hour:Int) -> Food? {
        if hour % 2 == 0 {
            let food = Food()
            return food
        }
        else {
            return nil
        }
    }
    
}
var cuteBaby10:Baby10? = Baby10()

if let baby = cuteBaby10 {
    let eatFood = baby.eat(2)
    if let food = eatFood {
        let message = "寶寶吃\(food.name)"
    }
}
cuteBaby10?.eat(2)
cuteBaby10 = nil
cuteBaby10?.eat(2)

cuteBaby10 = Baby10()
var name = cuteBaby10?.eat(2)?.name
cuteBaby10 = nil
name = cuteBaby10?.eat(2)?.name

class Baby11 {
    var age = 1
}
var cuteBaby11:Baby11? = Baby11()
var age = cuteBaby11?.age
//age = age + 1



/*

class Food {

}

class Dog {
var food = Food()
}

class Baby {
var dog1 = Dog()
var dog2:Dog? = Dog()
}

var cuteBaby:Baby? = Baby()
cuteBaby?.dog1.food
cuteBaby?.dog2?.food

cuteBaby?.dog2!
var dog = cuteBaby?.dog1
dog!
cuteBaby?.dog1!

*/

