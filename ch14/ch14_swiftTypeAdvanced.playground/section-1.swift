
// 14-1
var number:Any = "18"
number = 18

class Boy {
    
}

class Girl {
    
}

struct Monkey {
    
}

var people:AnyObject = Boy()
people = Girl()
//people = Monkey()

// 14-2
var number1:Any = 18
var number2:Int = 18
number1 = number2
//number2 = number1

var name1:String = "peter pan"
name1.hasPrefix("p")
var name2:Any = "peter pan"
//name2.hasPrefix("p")

// use as! on Xcode 6.3
number2 = number1 as Int

// use as! on Xcode 6.3
(name2 as String).hasPrefix("p")

// use as! on Xcode 6.3
var name3:Any? = "peter"
var name4 = name3 as String?
var name5 = name3 as String!
var name6 = name3 as String

// 14-3
var name:Any = "peter pan"
if name is String {
    (name as String).hasPrefix("p")
}

class Baby {
    
}

class SuperBaby:Baby {
    
}

var cuteBaby:Any = SuperBaby()
if cuteBaby is Baby {
    var message = "我身上流著寶寶的紅色血液"
}
else {
    var message = "我身上流著火星人的藍色血液"
}

// 14-4
var data:Any = "peter pan"
var age = data as? Int
var dataName = data as? String
dataName!.hasPrefix("p")

// 14-5
var dataArray:[Any] = [Boy(), Girl()]
var dataDictionary:[String:AnyObject] = ["boy":Boy(), "girl":Girl()]

class Baby2 {
    func cry() {
        
    }
}
var babyArray:[AnyObject] = [Baby2(), Baby2()]
for baby in babyArray {
    // use as! on Xcode 6.3
    var cuteBaby = baby as Baby2
    cuteBaby.cry()
}

// use as! on Xcode 6.3
for baby in babyArray as [Baby2] {
    baby.cry()
}

class Boy2 {
    var name = "潘安"
}

class Girl2 {
    var name = "趙飛燕"
}

var dataArray2:[Any] = [Boy2(), Girl2()]
for data in dataArray2 {
    if let boy = data as? Boy2 {
        var message = "我是天塌下來有我頂的\(boy.name)"

    }
    else if let girl = data as? Girl2 {
        var message = "我是如同羞答答的玫瑰的\(girl.name)"

    }
}

var people2:Any = Boy2()
switch people2 {
case let boy as Boy2:
    var message = "我是天塌下來有我頂的\(boy.name)"
case let girl as Girl2:
    var message = "我是如同羞答答的玫瑰的\(girl.name)"
default:
    var message = "我是雌雄同體的火星人"
}

// 14-6
class Baby3 {
    struct Dog  {
        enum Color {
            case Black, White
        }
    }
}
var cuteBaby3 = Baby3()
var cuteDog = Baby3.Dog()
var dogColor = Baby3.Dog.Color.White
//var handsomeDog = Dog()


