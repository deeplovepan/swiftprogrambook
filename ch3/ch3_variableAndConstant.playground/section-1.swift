
// 3-1
var age = 18
age = age + 1

// 3-2
var age1 = 18
var age2 = 18;

// 3-3
var 身高 = 180
var 體重 = 70
var 😄😍 = 100

// 3-4
var str = "很久很久很久很久很久很久以前"

// 3-5
// Var number = 3

// 3-6
let age3 = 18
//age3 = age3 + 1

// 3-8
var age4 = 18;
//age4 = "hello"

// 3-9
var age5:Int = 18
var name:String = "彼得潘"
//var age6:Int = 20.2

// 3-10
var num1 = 5

var num2:Int
num2 = 3
num1 = num2*10

var num3:Int
//num2 = num3

//var num4
//num4 = 5

let num5 = 5
//let num6:Int




