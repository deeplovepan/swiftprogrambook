import Foundation

// 15-1
protocol Idol {
    func sing()
    
    var height:Double {
        get
        set
    }
}

class Baby:Idol {
    func sing() {
        var song = "給我一杯忘情水"
    }
    var height = 180.5
}
var cuteBaby = Baby()

class Baby2:Idol {
    func sing() {
        var song = "給我一杯忘情水"
    }
    var height:Double {
        get {
            return 180.5
        }
        set {
            
        }
    }
}

protocol Butterfly {
    func fly()
}

class Baby3:Idol, Butterfly {
    func sing() {
        var song = "給我一杯忘情水"
    }
    
    func fly() {
        var message = "蝴蝶飛呀!飛向未來的城堡"
    }
    var height = 180.5
}

class Monkey {
    func eat() {
        
    }
}

class Baby4:Monkey, Idol {
    
    func sing() {
        var song = "給我一杯忘情水"
    }
    
    var height = 180.5
}

protocol Idol2 {
    func sing()
}

class Man:Idol2 {
    
    func cry() {
        var message = "男人哭吧哭吧哭吧不是罪"

    }
    
    func sing() {
        var song = "給我一杯忘情水"

    }
}
var handsomeMan:Idol2 = Man()
handsomeMan.sing()
// use as! on Xcode 6.3
(handsomeMan as Man).cry()

@objc protocol Idol3 {
    func sing()
    optional var money:Double {
        get
    }
    optional func playPiano() -> String
}

class Baby5:Idol3 {
    // add @objc on Xcode 6.3
    // @objc func sing() {
    func sing() {
        var song = "給我一杯忘情水"
    }
}

var cuteBaby5:Idol3 = Baby5()
cuteBaby5.playPiano?()

class Baby6:Idol3 {
    // add @objc on Xcode 6.3
    // @objc func sing() {
    func sing() {
        var song = "給我一杯忘情水"
    }
    
    func playPiano() -> String {
        return "情有獨鐘"
    }
    
    var money = 22000.0
}

var cuteBaby6:Idol3 = Baby6()
var message = cuteBaby6.playPiano?()
message!
var money = cuteBaby6.money
money!

// use static on Xcode 6.3
protocol Idol4 {
    class func sing()
    class var height:Double {
        get
        set
    }
}

/*
// use static on Xcode 6.3
class Baby7:Idol4 {
    class func sing(){
        
    }
    class var height = 180.5
}
*/

// 15-2
class Baby8 {
    func walk() {
        
    }
}

extension Baby8 {
    func run() {
        
    }
}

extension Baby8 {
    func fly() {
        
    }
}


var cuteBaby8 = Baby8()
cuteBaby8.run()
cuteBaby8.fly()

extension Int {
    func repetitions(task:()->()) {
        for i in 0..<self {
            task()
        }
    }
}

10.repetitions({
  println("真正重要的東西，用眼睛是看不見的，必須要用你的心")
})






