
// 7-1
func eat() {
    var message = "吃飽才有力氣寫App"
}
eat()

// 7-2
func eat(name:String, price:Int) {
    var message = "花了$\(price)吃\(name)"
}
eat("珍奶", 700)
//eat("珍奶", "100元")

// 7-3
func eat(foodName name:String, foodPrice price:Int) {
    var message = "花了$\(price)吃\(name)"
}
eat(foodName:"珍奶", foodPrice:700)

// 7-4
func eat(name:String, foodPrice price:Int) {
    var message = "花了$\(price)吃\(name)"
}
eat("珍奶", foodPrice:700)

// 7-5
func eat(name:String, #price:Int) {
    var message = "花了$\(price)吃\(name)"
}
eat("珍奶", price:700)

// 7-6
func eat2(name:String, price:Int = 50) {
    var message = "花了$\(price)吃\(name)"
}
eat2("珍奶")
eat2("珍奶", price: 700)

// 7-7
func eat3(name:String, price:Int = 100) -> String {
    return "花了$\(price)吃\(name)"
}
var str = eat3("珍奶")

func test1() {
    
}
func test2() -> () {
    
}
func test3() -> Void {
    
}

// 7-8
typealias Grade = Int
var boyGrade:Grade = 100

// 7-9
func eat(#name:String, #price:Int) -> String {
    return "花了$\(price)吃\(name)"
}

func eat(#name2:String, #price2:Int) -> String {
    return "吃\(name2)花了$\(price2)"
}
eat(name:"珍奶", price: 100)
eat(name2:"牛排", price2: 1000)

func eat3(#name:String, #price:Int) -> String {
    return "花了$\(price)吃\(name)"
}

func eat3(#name:String, #price:String) -> String {
    return "吃\(name)花了$\(price)"
}

func eat3(#name:String, #price:Int) -> Int {
    return price
}
eat3(name:"珍奶", price:"100")
var str2:String = eat3(name:"珍奶", price: 100)
var price:Int = eat3(name:"珍奶", price: 100)

// 7-10
func movie(title:String, actors:String...) {
    var message = "電影\(title)由"
    for actor in actors {
        message = message + actor
    }
    message = message + "領銜主演"
}
movie("無間道", "劉德華", "梁朝偉")
movie("無間道")

// 7-11
var grade1 = 100
var grade2 = grade1
grade2 = 60
var message = "聰明的彼得潘考了\(grade1)分，愚蠢的虎克船長考了\(grade2)分"

// 7-12
func makeStudentHappy(var grade:Int) {
    grade = grade + 60
}
var grade = 10
makeStudentHappy(grade)
message = "經過期末佛心大放送後，分數變成\(grade)分"

func makeStudentHappy(inout grade:Int) {
    grade = grade + 60
}
grade = 30
var originalGrade = grade
makeStudentHappy(&grade)
message = "期末考大放送，小穎分數變成\(originalGrade)變成\(grade)"

// 7-13
func buyBook(name:String, price:Int) -> String {
    return "花了$\(price)買\(name)"
}
buyBook("死神的精確度", 300)
var buyBook2:(String, Int) -> String
buyBook2 = buyBook
buyBook2("聖鬥士星矢", 250)

func buyBook3() -> String {
    return "買書的都是文藝青年"
}
var buyBook4 = buyBook3
var buyBook5 = buyBook3()
buyBook4()
buyBook5
//buyBook5()

// 7-14
func happy() -> String {
    return "忘記了姓名的請跟我來，現在讓我們向快樂崇拜，放下了包袱的請跟我來，傳開去建立個快樂的時代"
}

func sad() -> String {
    return "每天這個時候，心都特別寂寞，在窗邊吹風淚會流"
}

func feeling(#mood:Int) -> () -> String {
    if mood > 0 {
        return happy
    }
    else {
        return sad
    }
}
var todayFeeling = feeling(mood: 3)
message = todayFeeling()

// 7-15
func addAdjective(adjective:String) -> (String) -> String {
    var superStr = "超級"
    func createSentence(name:String) -> String {
        var message = superStr + adjective + name
        superStr = superStr + "超級"
        return message
    }
    return createSentence
}

var goodAdjective = addAdjective("可愛的")
goodAdjective("女朋友")
goodAdjective("女朋友")
var badAdjective = addAdjective("可惡的")
badAdjective("情敵")

// 7-16
func eatAndExercise(sport:()->String)->String {
    return "大吃特吃後" + sport()
}

func playTableTennis()->String {
    return "打桌球，腳步像小兔一樣輕盈"
}

func playBasketball()->String {
    return "打籃球，長得像大樹一樣高"
}
eatAndExercise(playTableTennis)
eatAndExercise(playBasketball)

func swim(name:String)->String {
    return "游泳，游得像\(name)一樣快"
}
//eatAndExercise(swim)

// 7-17
func createMovieMessage(age:Int?, title:String) -> String? {
    var message:String?
    if age != nil {
        message = "\(age!)以上才可以觀賞\(title)"
    }
    else if title != "吸血鬼" {
        message = "剛出生的小寶寶也能觀賞\(title)"

    }
    return message
}
var message1 = createMovieMessage(nil, "小鬼當家")
var message2 = createMovieMessage(18, "色戒")
var message3 = createMovieMessage(nil, "吸血鬼")

/*
func getMovieDescription(title:String) -> String {
    if title == "鐵達尼號" {
        return "不看會死的好電影"
    }
    else {
        return nil
    }
}
*/
message1 = createMovieMessage(18, "色戒")
message2 = message1! + "，因為不能說的秘密"

func createMovieMessage2(age:Int!, title:String) -> String! {
    var message:String?
    if age != nil {
        message = "\(age!)以上才可以觀賞\(title)"
    }
    else if title != "吸血鬼" {
        message = "剛出生的小寶寶也能觀賞\(title)"
        
    }
    return message
}
var message4 = createMovieMessage2(18, "色戒")
var message5 = message4 + "，因為不能說的秘密"

// 7-18
var name = "彼得潘"
var age = 18
print("我的名字是\(name)，今年\(age)歲")
print("明年也依然會是\(age)歲")

name = "虎克船長"
age = 90
println("我的名字是\(name)，今年\(age)歲")
println("明年又老了一歲，變成\(age+1)歲")

// 7-20
func addGrade(grade:Int) -> (Int) -> Int {
    func add(studentGrade:Int) -> Int {
        return studentGrade + grade
    }
    return add
}
var add10 = addGrade(10)
grade = 70
add10(grade)
addGrade(10)(grade)

func addGrade2(grade:Int)(studentGrade:Int) -> Int {
    return studentGrade + grade
}
addGrade2(10)(studentGrade:grade)




