
// 5-1
var isRain = true
if isRain {
    var str = "雨天"
}
else {
    var str = "陰天"
}

var number = 10
if number > 0 {
    
}

var height = 180
var money = 22000

if height > 170 && money > 1000000 {
    var str = "人生勝利組"
}

var number1 = 10
var number2 = 20
if number1 != 0 && number2 != 0 {
    
}

//if isRain
//    var str = "雨天"


// 5-2
var name = "狐狸"

if name == "小王子" {
    var message = "每個大人都曾經是小孩，雖然只有少數人記得。"
}
else if name == "狐狸" {
    var message = "這就是我要告訴你的秘密，很簡單，只有用心靈，一個人才能看得清楚。真正的東西是眼睛看不見的"
}

// 5-3
var grade = 0

switch grade {
case 0:
    var message = "朽木不可雕也!"
case 100:
    var message = "莫札特再世"
default:
    var message = "普通人"
}

switch grade {
case 0:
    var message = "朽木不可雕也!"
case 100:
    break
default:
    var message = "普通人"
}


switch grade {
case 0:
    fallthrough
case 100:
    var message = "天才與白痴之間，只有一線之隔"
default:
    var message = "普通人"
}

switch grade {
case 0,100:
    var message = "天才與白痴之間，只有一線之隔"
default:
    var message = "普通人"
}

var myName = "彼得潘"
switch myName {
case "彼得潘":
    var message = "好人"
case "虎克船長":
    var message = "壞人"
default:
    var message = "路人"
}

// 5-4
var sum = 0
for var i=0;i<10;i++ {
    sum = sum + i
}

sum = 0
for var i=0;sum<100;i=i+10 {
    sum = sum + i
}

// 5-5
grade = 80
switch grade {
case 60...100:
    var str = "升級"
default:
    var str = "留級，再唸一年!"
}

sum = 0
for i in 1...10 {
    sum = sum + i
}
for i in 1..<10 {
    sum = sum + i
}

name = "彼得潘"
for char in name {
    char
}

// 5-6
number = 10
while number < 10 {
    number = number + 1
}

number = 10
do {
    number = number + 1
}while number < 10

// 5-7
/*
var lyrics1 = "匆匆那年，我們一時匆忙撂下，難以承受的諾言，只有等別人兌現"
/*
var lyrics2 = "如果過去還值得眷戀，別太快冰釋前嫌，誰甘心就這樣，彼此無掛也無牽"
*/
var lyrics3 = "不怪那天太冷，淚滴水成冰，春風也一樣，沒吹進凝固的照片"
*/





