
// 11-1
struct Baby {
    var age = 1
    var weight = 10.0
    
    func cry() {
        
    }
    
    static var maxAge = 200
    static func run() {
        
    }
}
var cuteBaby = Baby()
cuteBaby.weight = 12.0
cuteBaby.cry()
Baby.maxAge = 1000
Baby.run()

// 11-2
struct Baby2 {
    var age:Int
    var weight:Float
    
    func cry() {
        
    }
}
var cuteBaby2 = Baby2(age: 1, weight: 10.5)

struct Baby3 {
    var age:Int = 1
    var weight:Float = 10.5
    
    func cry() {
        
    }
}
var cuteBaby3 = Baby3()
cuteBaby3 = Baby3(age: 2, weight: 10.5)

struct Baby4 {
    var age:Int
    var weight:Float
    
    init() {
        age = 1
        weight = 10.5
    }
    
    func cry() {
        
    }
}
var cuteBaby4 = Baby4()
//cuteBaby4 = Baby4(age: 1, weight: 10.5)

// 11-3
struct Baby5 {
    var age:Int = 1
    var weight:Float = 10.5
}
var cuteBaby5 = Baby5()
var cuteBaby6 = cuteBaby5
cuteBaby5.age = 2
cuteBaby6.age

class Baby7 {
    var age:Int = 1
    var weight:Float = 10.5
}
var cuteBaby7 = Baby7()
var cuteBaby8 = cuteBaby7
cuteBaby7.age = 2
cuteBaby8.age

struct Baby9 {
    var age:Int = 1
    var weight:Float = 10.5
}
let cuteBaby9 = Baby9()
//cuteBaby9.age = 2

// 11-4
struct Baby10 {
    var age:Int = 1
    var weight:Float = 10.5
    
    mutating func increaseAge() {
        self.age = self.age + 1
    }
}

// 11-5
var name = ""
if name.isEmpty {
    var message = "無名的可憐人"
}
else {
    var message = "有名的幸運兒"
}

name = "李白"
if name.hasPrefix("李") {
    var message = "姓李的人"
}
else {
    var message = "其它人"
}

var ageString = "100"
var age = ageString.toInt()
ageString = "peter"
age = ageString.toInt()

var name1 = "Peter Pan"
var name2 = "Wendey"
if name1 < name2 {
    var message = "小於"
}
else {
    var message = "大於等於"
}





