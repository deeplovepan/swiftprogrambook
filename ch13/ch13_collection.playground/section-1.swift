
// 13-1
var names = ["張智霖", "劉德華", "梁朝偉"]
var firstName = names[0]

names[2] = "林隆璇"
names
names[1...2] = ["林俊傑"]
names

var names1 = ["張智霖", "劉德華", "梁朝偉"]
var names2:[String] = ["張智霖", "劉德華", "梁朝偉"]
var names3:Array<String> = ["張智霖", "劉德華", "梁朝偉"]

var names4 = [String]()
var names5 = Array<String>()
var names6:[String] = Array()
var names7:[String] = []

names = ["張智霖", "劉德華", "梁朝偉"]
names.append("林隆璇")
names.extend(["張信哲"])
names = names + ["林俊傑"]
names += ["VK克"]

names = ["張智霖", "劉德華", "梁朝偉"]
names.insert("林隆璇", atIndex: 1)

struct Baby {
    var name:String
}
var names8 = [Baby(name: "劉德華"), Baby(name: "梁朝偉")]
var names9 = names8
names9[0].name = "張智霖"
names9[1] = Baby(name: "林隆璇")
names8
names9

class Baby2 {
    var name:String
    
    init(name:String) {
        self.name = name
    }
}
var names10 = [Baby2(name: "劉德華"), Baby2(name: "梁朝偉")]
var names11 = names10
names11[0].name = "張智霖"
names11[1] = Baby2(name: "林隆璇")
names10
names11

names = ["張智霖", "劉德華", "梁朝偉", "林隆璇", "林俊傑"]
names.removeLast()
names
names.removeAtIndex(3)
names
names.removeRange(1...2)
names.removeAll()

// 13-2
names = ["張智霖", "劉德華", "梁朝偉"]
var count = names.count
if names.isEmpty {
    var message = "我沒有偶像"
}

var numbers = Array(1...10)

var grades = Array(count: 10, repeatedValue: 60)

for name in names {
    if name == "劉德華" {
        var message = "\(name) - 永遠的天王"

    }
}

var names12 = names.reverse()

// 13-3
var book = ["name":"小王子", "author":"聖修伯里"]
var bookName = book["name"]
var bookAuthor = book["author"]
var bookPrice = book["price"]

var book1 = ["name":"小王子", "author":"聖修伯里"]
var book2:[String:String] = ["name":"小王子", "author":"聖修伯里"]
var book3:Dictionary<String, String> = ["name":"小王子", "author":"聖修伯里"]

var book4 = [String:String]()
var book5 = Dictionary<String, String>()
var book6:[String:String] = Dictionary()
var book7:[String:String] = [:]

book["price"] = "300元"
book["name"] = "小公主"
var oldValue = book.updateValue("小紅帽", forKey: "name")
book

var book8 = book1
book8["name"] = "小公主"
book1
book8

book = ["name":"小王子", "author":"聖修伯里", "price":"300元"]
book["name"] = nil
book.removeValueForKey("author")
book
book.removeAll()

// 13-4
class BabyToy {
    var toys = ["聖鬥士星矢", "小叮噹"]
    
    subscript(index:Int) -> String {
        get {
            return toys[index]
        }
        set(toyName) {
            toys[index] = toyName
        }
    }
}

var myBabyToy = BabyToy()
var firstToy = myBabyToy[0]
myBabyToy[0] = "大富翁"
firstToy = myBabyToy[0]

class BabyToy2 {
    var toys = ["聖鬥士星矢", "小叮噹"]
    
    subscript(index:Int) -> String {
        get {
            return toys[index]
        }
        set {
            toys[index] = newValue
        }
    }
}

class MultiplicationTable {
    
    subscript(number1:Int, number2:Int) -> Int {
        get {
            return number1*number2
        }
    }
}

var multiplicationTable = MultiplicationTable()
var result = multiplicationTable[3,3]
result = multiplicationTable[3,5]

// 13-5
var food = ("菲力牛排", "茹絲葵", 10000)
var message = "彼得潘花了\(food.2)元，在\(food.1)和溫蒂吃\(food.0)"

food.1 = "王品"
food
food = ("小籠包", "鼎泰豐", 300)

var food1:(String, String, Int) = ("菲力牛排", "茹絲葵", 10000)

var food2 = (name:"菲力牛排", restaurant:"茹絲葵", price:10000)
message = "彼得潘花了\(food2.price)元，在\(food2.restaurant)和溫蒂吃\(food2.name)"

var food3:(name:String, restaurant:String,
  price:Int) = ("菲力牛排", "茹絲葵", 10000)
message = "彼得潘花了\(food3.price)元，在\(food3.restaurant)和溫蒂吃\(food3.name)"

var (foodName, foodRestaurant, foodPrice) = food3
message = "彼得潘花了\(foodPrice)元，在\(foodRestaurant)和溫蒂吃\(foodName)"

// 13-6
var foodDic = ["name":"菲力牛排", "price":"1000"]
for (key, value) in foodDic {
    var message = "\(key) \(value)"
}

var foods = ["菲力牛排", "松阪豬"]
for (index ,element) in enumerate(foods) {
    var message = "\(index) \(element)"
}

// 13-7
func eat() -> (String, Int) {
    return ("菲力牛排", 1000)
}
var foodInfo = eat()
message = "一口\(foodInfo.1/10)塊的\(foodInfo.0)"

// 13-8
var food4 = ("菲力牛排", 1000)
switch food4 {
case (_, 1000):
    var message = "1000元的大餐"
default:
    var message = "不到一千元的小餐"
}

switch food4 {
case let (x, y) where y>=1000:
    var message = "吃不飽的精緻美食:\(x)"
default:
    var message = "便宜又大碗的夜市小吃"
}

switch food4 {
case let ("蝸牛", y) where y<100:
    var message = "吃不飽的便宜蝸牛"
default:
    var message = "只要不是便宜的蝸牛就好"
}


