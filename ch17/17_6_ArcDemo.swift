
class Baby {
    
}

class Rabbit {
    let name: String
    
    
    var actionClosure: (() -> ())!
    
    init(name: String) {
        self.name = name
        
        self.actionClosure = { [weak self] in
            if self != nil {
                println("我是全天下最可愛的\(self!.name)")
            }
        }
    }
    
    
    func performAction() {
        actionClosure()
    }
    deinit {
        println("死了都要愛")
    }
}

var cuteRabbit:Rabbit? = Rabbit(name: "彼得兔")
cuteRabbit?.actionClosure()
cuteRabbit = nil

