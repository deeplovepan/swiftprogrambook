
// 17-1
class People {
    var name:String
    init(name:String) {
        self.name = name
    }
}

func + (people1:People, people2:People) -> People {
    return People(name: "彼得潘")
}

var aMan = People(name: "亞當")
var aWoman = People(name: "夏娃")
var aBaby = aMan + aWoman

// 17-2
prefix func ~ (people:People) -> People {
    return People(name: people.name + "大大")
}
var superMan = ~aMan

// 17-3
infix operator ***** {}

func *****(people1:People, people2:People) -> People {
    return People(name: "彼得潘")
}
var aBaby2 = aMan ***** aWoman

