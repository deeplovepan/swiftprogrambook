

class Baby {
    weak var pet:Dog?
    deinit {
        println("人生自古誰無死，留取丹心照汗青")
    }
}

class Dog {
    var owner:Baby?
    deinit {
        println("主人，我即將死去，謝謝你愛過我")
    }
}
var cuteBaby:Baby? = Baby()
var cuteDog:Dog? = Dog()
cuteDog?.owner = cuteBaby
cuteBaby?.pet = cuteDog
println("我親愛的\(cuteBaby?.pet)")
cuteDog = nil
println("我親愛的\(cuteBaby?.pet)")
cuteBaby = nil
