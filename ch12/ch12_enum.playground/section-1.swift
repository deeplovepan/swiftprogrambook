
// 12-1
// dog is 0, cat is 1, rabbit is 2
var myPet = 2
if myPet == 2 {
    println("我是比史努比更可愛的天下第一狗")
}

// 12-2
enum Pet {
    case Dog, Cat, Rabbit
}
var myPet2 = Pet.Rabbit
myPet2 = .Rabbit

enum 楊過武功絕學 {
    case 玉女劍法, 全真劍法, 玄鐵重劍劍法, 黯然銷魂掌
}

enum Pet2 {
    case Dog
    case Cat
    case Rabbit
}

// 12-3
switch myPet2 {
case .Dog:
    println("小狗是最忠心的")
case .Cat:
    println("小貓是最貼心的")
case .Rabbit:
    println("小兔是最可愛的")
}

// 12-4
enum Pet3:String {
    case Dog = "小狗", Cat = "小貓", Rabbit = "小兔"
}
var myPet3 = Pet3.Rabbit
var message = "可愛的\(myPet3.rawValue)"
var myPet4 = Pet3(rawValue: "小兔")
message = "可愛的\(myPet4!.rawValue)"
myPet4 = Pet3(rawValue: "小龍")

enum Grade:Int {
    case A = 1, B, C
}

// 12-5
enum Status {
    case OnTime
    case Delayed(min:Int, sec:Int)
}
var todayStatus = Status.Delayed(min: 10, sec: 20)

switch todayStatus {
case .OnTime:
    var message = "加薪一百萬"
case .Delayed(let min, let sec):
    if min > 30 {
        var message = "明天不用來了"
    }
    else {
        var message = "扣薪一百萬"
    }
}

switch todayStatus {
case .OnTime:
    var message = "加薪一百萬"
case let .Delayed(min, sec):
    if min > 30 {
        var message = "明天不用來了"
    }
    else {
        var message = "扣薪一百萬"
    }
}

switch todayStatus {
case .OnTime:
    var message = "加薪一百萬"
case .Delayed(var min, var sec):
    min = min + 100
    if min > 30 {
        var message = "明天不用來了"
    }
    else {
        var message = "扣薪一百萬"
    }
}

class Baby {
    var name:String?
}

enum Status2 {
    case OnTime
    case Delayed(min:Int, sec:Int, baby:Baby)
}

// 12-6
enum Animal:String {
    case Dog = "小狗", Cat = "小貓", Rabbit = "小兔"
    init() {
        self = Cat
    }
}
var myAnimal = Animal()

var myAnimal1 = Animal.Cat
var myAnimal2 = myAnimal1
message = "我的兩個心肝寶貝，\(myAnimal1.rawValue)和\(myAnimal2.rawValue)"
myAnimal1 = Animal.Dog
message = "我的兩個心肝寶貝，\(myAnimal1.rawValue)和\(myAnimal2.rawValue)"





