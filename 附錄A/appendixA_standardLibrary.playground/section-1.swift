
// A-1
var grades = [50, 70, 80]
var newGrades:[Int] = grades.map { grade in
    return grade + 10
}
newGrades = map(grades, { grade in
    return grade + 10
})

// A-2
var sum = grades.reduce(0, combine: { gradeSum, grade in
        return gradeSum + grade
})

sum = reduce(grades, 0, { gradeSum, grade in
    return gradeSum + grade
})

// A-3
var goodGrades = grades.filter{ grade in
    return grade >= 60
}

goodGrades = filter(grades, { grade in
    return grade >= 80
    
})

// A-4
for i in stride(from:10, through:0, by:-2) {
    println("\(i)")
}

for i in stride(from:10, to:0, by:-2) {
    println("\(i)")
}

// A-5
grades.sort{
    $0 > $1
}
grades
grades.sorted{
    $0 < $1
}
grades

sort(&grades, {
    $0 < $1
})
grades
sorted(grades, {
    $0 > $1
})
grades






