//
//  Baby.m
//  AppDemo
//
//  Created by Peter Pan on 1/31/15.
//  Copyright (c) 2015 Peter Pan. All rights reserved.
//

#import "Baby.h"
#import "AppDemo-Swift.h"

@implementation Baby

-(void)cry {
    
    Car *myCar = [[Car alloc] init];
    [myCar run];
    
}

@end
