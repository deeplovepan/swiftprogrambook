//
//  Baby.h
//  AppDemo
//
//  Created by Peter Pan on 1/31/15.
//  Copyright (c) 2015 Peter Pan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Baby : NSObject

-(void)cry;

@end
