//
//  ViewController.swift
//  AppDemo
//
//  Created by Peter Pan on 1/31/15.
//  Copyright (c) 2015 Peter Pan. All rights reserved.
//

import UIKit


#if DEBUG1
    
#else
    
#endif

@objc class Car:NSObject {
    
    func run() {
        
    }
}

class ViewController: UIViewController {

    func butPressed(sender:AnyObject) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let yellowView = UIView(frame: CGRectMake(0, 0, 100, 100))
        
        let color = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        
        let label = UILabel()
        label.textAlignment = .Center
        
        let but = UIButton()
        but.setTitle("按我", forState: .Normal)
        but.addTarget(self, action: "butPressed:", forControlEvents: .TouchUpInside)
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
        })
        
        let when = dispatch_time(DISPATCH_TIME_NOW, Int64(5*NSEC_PER_SEC))
        dispatch_after(when, dispatch_get_main_queue()){
        }
        
        let tableView = UITableView(frame: CGRectMake(0, 0, 100, 100))
        tableView.registerClass(DemoTableViewCell.self , forCellReuseIdentifier: "DemoTableViewCellId")

        var cuteBaby = Baby()
        cuteBaby.cry()
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

