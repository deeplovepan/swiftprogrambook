
// 8-2
let aquariusIntro = { ()->() in
    var introMessage = "1.不喜歡按排理出牌。2.碰到有趣的事情，經常捨不得錯過。3.耐不住性子的好奇寶寶。"
}
aquariusIntro()

let aquariusIntro2 = aquariusIntro
aquariusIntro2()

/*
let aquariusIntro3:()->String = { ()->() in
    var introMessage = "1.不喜歡按排理出牌。2.碰到有趣的事情，經常捨不得錯過。3.耐不住性子的好奇寶寶。"
}
*/

let aquariusIntro4:()->() = {
    var introMessage = "1.不喜歡按排理出牌。2.碰到有趣的事情，經常捨不得錯過。3.耐不住性子的好奇寶寶。"
}
aquariusIntro4()

let aquariusIntro5:()->String = {
    return "1.不喜歡按排理出牌。2.碰到有趣的事情，經常捨不得錯過。3.耐不住性子的好奇寶寶。"
}
var message = aquariusIntro5()

// 8-3
func repeat(count:Int, task:()->()){
    for i in 0..<count {
        task()
    }
}

func eat() {
    var message = "吃飯皇帝大"
}

func sleep() {
    var message = "睡覺皇后大"
}

repeat(3, eat)
repeat(3, sleep)

repeat(3, {
    var message = "吃飯皇帝大"
})
repeat(3, {
    var message = "睡覺皇后大"
})

// 8-4
repeat(3) {
    var message = "吃飯皇帝大"
}

// 8-5
func repeat(task:()->()){
    for i in 0..<10 {
        task()
    }
}
repeat{
    var message = "吃飯皇帝大"
}

// 8-6
let listen = { (singer:String, song:String) -> () in
      var message = "聽\(singer)唱\(song)"
}
listen("蔡淳佳", "陪我看日出")

func loveEvent(count:Int, event:(Int, String) -> ()) {
    for i in 1...count {
        event(i, "沈佳宜")
    }
}
loveEvent(2, { (index:Int, name:String) -> () in
    var message = "第\(index)次和\(name)賞月數星星"
})
loveEvent(2, { (index:Int, name:String) -> () in
    var message = "第\(index)次和\(name)睡覺數綿羊"
})

// 8-7
loveEvent(2, { index, name -> () in
    var message = "第\(index)次和\(name)賞月數星星"
})

// 8-8
loveEvent(2, { index, name in
    var message = "第\(index)次和\(name)賞月數星星"
})

// 8-9
loveEvent(2, {
    var message = "第\($0)次和\($1)賞月數星星"
})

// 8-10
func loveEvent(event:(String) -> String) {
    var message = event("沈佳宜")
}

loveEvent({ name -> String in
    return "每個人心中，都有一個\(name)"
})

loveEvent({ name in
    "每個人心中，都有一個\(name)"
})





