
// 4-1
var number = 1_000_000
var doYouLoveMe = true
//var doYouLoveMe2:Bool = 1

// 4-2
//var overflow = Int.max + 1

// 4-3
var num1 = 2
var num2 = 3.2
//var num3 = num1 + num2
var num3 = Double(num1) + num2
var num = 3 + 2.5

// 4-4
var age = 18
var name = "彼得潘"
var height = 180.5
var intro = "我的名字是\(name)，今年\(age+2)歲，身高\(height)"

var str1 = "我只好假裝我看不到"
var str2 = "看不到你和她在對街擁抱"
var str3 = "\(str1)，\(str2)"
var str4 = str1 + "，" + str2





