
// 16-1
class Fox {
    func run(){
        
    }
}

class LittlePrince {
    var pet:Fox?
}

var prince = LittlePrince()
prince.pet = Fox()

class LittlePrinceForRabbit {
    var pet:Rabbit?
}

class Rabbit {
    func run(){
        
    }
}

var prince2 = LittlePrinceForRabbit()
prince2.pet = Rabbit()

// 16-2
class LittlePrince2 {
    var pet:Any?
}
var prince3 = LittlePrince2()
prince3.pet = Fox()
// use as! on Xcode 6.3
(prince3.pet as Fox).run()
prince3.pet = Rabbit()

class Zombie {
    func bite() {
        
    }
}
prince3.pet = Zombie()

// 16-3
class LittlePrince3 <T> {
    var pet:T?
}
var prince4 = LittlePrince3<Fox>()
prince4.pet = Fox()
prince4.pet?.run()

var prince5:LittlePrince3<Fox>
prince5 = LittlePrince3<Fox>()
//prince5 = LittlePrince3<Rabbit>()

class LittlePrince4 <T1, T2> {
    var pet1:T1?
    var pet2:T2?
}

var prince6 = LittlePrince4<Fox, Rabbit>()
prince6.pet1 = Fox()
prince6.pet2 = Rabbit()

// 16-4
protocol Cute {
    func smile()
}

class Fox2:Cute {
    func smile() {
        
    }
}

class LittlePrince5 <T:Cute> {
    var pet:T?
}

var prince7 = LittlePrince5<Fox2>()
//var prince8 = LittlePrince5<Zombie>()

protocol Quiet {
    func sleep()
}

class Animal {
    func eat() {
        
    }
}

class Fox3:Animal, Cute, Quiet {
    

    func smile(){

    }
    
    func sleep() {
        
    }
}

class LittlePrince6<T:Animal where T:Quiet, T:Cute> {
    var pet:T?
    
}

var prince9 = LittlePrince6<Fox3>()

// 16-5
func play<T:Animal where T:Cute>(pet:T) {
    pet.smile()
    pet.eat()
}
var pet = Fox3()
play(pet)










