import UIKit

// B-2
var nameSet = Set(["郭靖", "黃蓉"])
nameSet.insert("洪七公")
nameSet.insert("郭靖")
var name = nameSet.first

var nameSet1 = Set(["郭靖", "黃蓉"])
var nameSet2 = Set(["楊過", "黃蓉"])
nameSet1.intersect(nameSet2)
nameSet1.union(nameSet2)

// B-3
var mathGrade:Int? = 90
var englishGrade:Int? = 70
var location = "台灣"
var age = 18

if location == "台灣" && age == 18, let mathGrade = mathGrade, englishGrade = englishGrade where mathGrade > 60 &&
    englishGrade > 60 {
        var message = "來自台灣，英數雙全的少年英才"
}

// B-4
func listenMusic(isRain:Bool) {
    let song:String
    if isRain {
        song = "終於聽見下雨的聲音"
    }
    else {
        song = "等到放晴的那天，也許我會比較好一點"
    }
}
listenMusic(true)

// B-5
class Baby {
    static var maxAge:Int = 200
    static func cry() {
        
    }
}

// B-6
class SuperBaby:Baby {
    
}
var cuteBaby1:AnyObject = Baby()
var cuteBaby2 = cuteBaby1 as! Baby
//var cuteBaby3 = cuteBaby1 as Baby
var cuteBaby4 = SuperBaby()
var cuteBaby5 = cuteBaby4 as Baby

// B-7
var name1:String = "彼得潘"
var name2:NSString = "虎克船長"
var name3:NSString = name1
var name4:String = name2 as String
//var name5:String = name2

// B-9
@objc enum Color:Int {
    case Black, White, Blue
}





